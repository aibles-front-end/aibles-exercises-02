
//-------- bài 1---------
let objects = [
    {number: 45 },
    {number: 4  },
    {number: 9  },
    {number: 16 },
    {number: 25 },
    {number: 16 },
    {number: 24 },
];


// map
let mapArr = objects.map(object => object.number ); 
console.log(mapArr )

// filter
let filterArr = mapArr.filter(x => x < 20);
console.log(filterArr);

// reduce

var sum = mapArr.reduce((a,b) => a+b);
console.log(sum);



// -----------bài 2-----------
function formatMoney(number){
    if(number<=0) return 0;
    else return number.toLocaleString();
};
console.log(formatMoney(1234567890))



//----------- bài3-----------
var arr = [1, [2, [3, [4, [5, 6,7]]]]]
var str = arr.join()
if(str.search('7')!=-1) console.log("Boom!")
else console.log("there is no 7 in the array")



// -----------bài 4------------
let str = "    the sky is blue    "
let arrWords = str.trim().split(" ").reverse()
let output = ""
arrWords.forEach( x => output+= x +' ');
console.log(output)



//------------bài 5------------
let arr = [ 1,2,[3,4,5],
            [1,2,3,4,5],
            [1,2,3,4,5]
        ]
console.log(arr.flat(Infinity).length)



// -----------bài 8------------
function isPandigital(number){
    let str = number.toString()
    for(var i=0;i<=9;i++){
        if(str.search(i)==-1) return false;
    }
    return true
}
console.log(isPandigital(1122334455667890))



// ------------bài 9------------
function howUnlucky(year){
    let dem=0,month=0;
    while ( month++ < 12 ){
        var d = new Date( year  + "-" + month + "-13"); // YYYY-MM-DD
        if(d.getDay()== 5 ) dem++;
    }
    return dem;
}
console.log(howUnlucky(2015))


