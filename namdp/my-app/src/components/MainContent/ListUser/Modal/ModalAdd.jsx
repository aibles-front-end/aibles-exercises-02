import { useState } from "react";
import callApi from "../../../../callApi/ApiCaller";
import "./Modal.scss"

export default function ModalAdd(props) {
  const [name,setName] = useState("")
  const [email,setEmail] = useState("")
  const [city,setCity] = useState("")
  const [message,setMessage] =useState("")

  const onSave = (e) => {
    e.preventDefault();
    callApi("users", "POST", {
      name,
      email,
      city
    }).then((res) => {
      props.handleSubmitAdd(res.data)
        setMessage(`You have been created successful user: ${name}!!`)
        setName("")
        setEmail("")
        setCity("")
    }).catch((res) =>{
      
    })
  };


  return (
    <div>
      <button
        type="button"
        className="btn btn-primary"
        data-bs-toggle="modal"
        data-bs-target="#modal"
      >
        Add user
      </button>
      <div
        className="modal fade"
        id="modal"
        aria-labelledby="modal-label"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="modal-label">
                  Add user
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="close"
              ></button>
            </div>
            <p className="message__Add">{message}</p>
            <div className="modal-body text-center">
              <form onSubmit={onSave}>
                <div className="form-group">
                  <label>Name: </label>
                  <input
                    type="text"
                    placeholder="name"
                    name="name"
                    onChange={(e) => {setName(e.target.value)}}
                    required
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="" className="me-3 mt-4">
                    City:
                  </label>
                  <input
                    type="text"
                    placeholder="city"
                    name="city"
                    onChange={(e) => {setCity(e.target.value)}}
                    required
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="" className="me-1 mt-4 mb-5">
                    Email:
                  </label>
                  <input
                    type="text"
                    placeholder="email"
                    name="email"
                    onChange={(e) => {setEmail(e.target.value)}}
                    required
                  />
                </div>
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Cancel
                </button>
                <input
                  type="submit"
                  className="btn btn-primary ms-5 px-4"
                  value="Add"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
