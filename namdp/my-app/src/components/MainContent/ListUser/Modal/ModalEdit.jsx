import { useEffect, useState } from "react";
import callApi from "../../../../callApi/ApiCaller";

export default function ModalEdit(props) {
  const [name,setName] = useState("")
  const [email,setEmail] = useState("")
  const [city,setCity] = useState("")

  useEffect(() => {
    if(props.infoUser.name!==name){
      setName(props.infoUser.name)
    }

    if(props.infoUser.email!==email){
      setEmail(props.infoUser.email)
    }

    if(props.infoUser.city!==city){
      setCity(props.infoUser.city)
    }
  },[props.infoUser])

  const onSave = (e) => {
    e.preventDefault();
    callApi(`users/${props.infoUser.id}`, "PUT", {
      name,
      email,
      city
    }).then((res) => {
      props.handleSubmitEdit(res.data,props.index)
    })
  };


  return (
    <div>
      <div
        className="modal fade"
        id="modalEdit"
        aria-labelledby="modal-label"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="modal-label">
                  Edit user
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="close"
              ></button>
            </div>
            <div className="modal-body text-center">
              <form onSubmit={onSave}>
                <div className="form-group">
                  <label>Name: </label>
                  <input
                    type="input"
                    placeholder="name"
                    name="name"
                    onChange={(e) => {setName(e.target.value)}}
                    defaultValue = {name}
                    required
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="" className="me-3 mt-4">
                    City:
                  </label>
                  <input
                    type="text"
                    placeholder="city"
                    name="city"
                    defaultValue = {city}
                    onChange={(e) => {setCity(e.target.value)}}
                    required
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="" className="me-1 mt-4 mb-5">
                    Email:
                  </label>
                  <input
                    type="text"
                    placeholder="email"
                    name="email"
                    defaultValue = {email}
                    onChange={(e) => {setEmail(e.target.value)}}
                    required
                  />
                </div>
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Cancel
                </button>
                <input
                  type="submit"
                  className="btn btn-primary ms-5 px-4"
                  value="Update"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
