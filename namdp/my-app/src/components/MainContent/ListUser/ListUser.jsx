import React, { useEffect, useState } from "react";
import callApi from "../../../callApi/ApiCaller";
import ModalAdd from "./Modal/ModalAdd";
import ModalEdit from "./Modal/ModalEdit";
import "./ListUser.scss";

export default function ListUser() {
  const [listUser, setListUser] = useState([]);
  const [user, setUser] = useState({});
  const [index, setIndex] = useState(null);

  useEffect(() => {
    callApi("users", "GET", null).then((res) => {
      setListUser(res.data);
    });
  }, []);

  const handleDeleteUser = (id) => {
    if (confirm("You are sure delete user?")) {//eslint-disable-line
      callApi(`users/${id}`, "DELETE", null).then((res) => {
        if (res.status === 200) {
          console.log("You has been deleted successful!");
          const i = listUser.filter((item) => item.id !== id);
          setListUser(i);
        }
      });
    }
  };

  const handleSubmitAdd = (data) => {
    console.log(data);
    setListUser([...listUser, data]);
  };

  const handleSubmitEdit = (data, index) => {
    const arr = [...listUser]
    arr[index] =data
    setListUser(arr)
  };

  const takeInfoUser = (user,index) => {
    setUser(user);
    setIndex(index)
  };
  // spread operator

  return (
    <div>
      <ModalAdd handleSubmitAdd={handleSubmitAdd} />

      <table className="table table-hover table-bordered text-break text-center">
        <thead>
          <tr>
            <th>id</th>
            <th>Name</th>
            <th>City</th>
            <th>Email</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody className="table-user">
          {listUser.map((item, index) => (
            <tr key={item.id}>
              <th>{index + 1}</th>
              <th>{item.name}</th>
              <th>{item.city}</th>
              <th>{item.email}</th>
              <th>
                <button
                  className="border table-user__btn"
                  onClick={() => handleDeleteUser(item.id)}
                >
                  Delete
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  data-bs-toggle="modal"
                  data-bs-target="#modalEdit"
                  onClick={() => takeInfoUser(item,index)}
                >
                  edit user
                </button>
              </th>
            </tr>
          ))}
          <ModalEdit 
            infoUser={user} 
            index={index}
            handleSubmitEdit={handleSubmitEdit} />
        </tbody>
      </table>
    </div>
  );
}
