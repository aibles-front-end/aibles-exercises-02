import ListUser from "./ListUser/ListUser";
import "./MainContent.scss";


export default function MainContent() {
  return (
    <div className="container main__table">
      <div className="content__table border-top border-3 p-3 rounded mb-5">
        <div className="d-flex justify-content-between mb-4">
          <h5 className="mt-1">List user</h5>
        </div>
        <ListUser />
      </div>
    </div>
  );
}
